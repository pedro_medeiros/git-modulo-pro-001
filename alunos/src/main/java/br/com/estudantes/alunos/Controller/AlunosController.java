package br.com.estudantes.alunos.Controller;

import java.util.ArrayList;
import java.util.List;
import br.com.estudantes.alunos.Alunos;
import br.com.estudantes.alunos.Exceptions.AlunoNaoEncontradoException;
import br.com.estudantes.alunos.Service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Alunos")
public class AlunosController {
	
	private final List<Alunos> aluno;

	@Autowired
	private AlunoService AlunoService;

	public AlunosController() {
		this.aluno = new ArrayList<Alunos>();
		Alunos aluno1 = new Alunos(01, "Ricardo", 17);
		Alunos aluno2 = new Alunos(02, "Amanda", 19);
		Alunos aluno3 = new Alunos(03, "Lucas", 18);
		aluno.add(aluno1);
		aluno.add(aluno2);
		aluno.add(aluno3);
	}
	@GetMapping
	public List<Alunos> findAll(@RequestParam(required = false) Alunos alunos) {

		return AlunoService.findAll(alunos);

	}
	@PostMapping
	public ResponseEntity<Integer> add(@RequestBody final Alunos alunos) {
		Integer id = AlunoService.add(alunos);
		return new ResponseEntity<>(alunos.getId(), HttpStatus.CREATED);

	}
	@PutMapping
	public ResponseEntity update(@RequestBody final Alunos alunos) {
		AlunoService.update(alunos);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable("id") Integer id) {
		AlunoService.delete(id);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
}
	


