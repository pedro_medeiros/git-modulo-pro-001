package br.com.estudantes.alunos.Exceptions;

public class AlunoNaoEncontradoException extends Exception {
    public AlunoNaoEncontradoException(final String nome){
        super("Nome" + nome + "Não encontrado!");
    }
}
