package br.com.estudantes.alunos.Exceptions.;

public class IdNaoEncontradoException extends Exception{
    public IdNaoEncontradoException(final Integer id){
        super("id" + id + "Não encontrado!");
    }

}
