package br.com.estudantes.alunos.Service;


import br.com.estudantes.alunos.Alunos;
import br.com.estudantes.alunos.Exceptions.AlunoNaoEncontradoException;
import br.com.estudantes.alunos.Exceptions.IdNaoEncontradoException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class AlunoService {

    private List<Alunos> aluno;

    public AlunoService() {
        this.aluno = new ArrayList<Alunos>();
        Alunos aluno1 = new Alunos(01, "Ricardo", 17);
        Alunos aluno2 = new Alunos(02, "Amanda", 19);
        Alunos aluno3 = new Alunos(03, "Lucas", 18);
        aluno.add(aluno1);
        aluno.add(aluno2);
        aluno.add(aluno3);
    }

        public List<Alunos> findAll (Alunos alunos){
            if (alunos.getNome() != null) {
                return aluno.stream()
                        .filter(aln -> aln.getNome().contains(alunos.getNome()))
                        .collect(Collectors.toList());
            }
            if (alunos.getIdade() != null) {
                return aluno.stream()
                        .filter(aln -> aln.getIdade().equals(alunos.getIdade()))
                        .collect(Collectors.toList());
            }
            if (alunos.getId() != null) {
                return aluno.stream()
                        .filter(aln -> aln.getId().equals(alunos.getId()))
                        .collect(Collectors.toList());
            }
            return aluno;
        }

    public Integer add (Alunos alunos){
            if (alunos.getId() == null) {
                alunos.setId(aluno.size() + 1);
            }
            aluno.add(alunos);
            return alunos.getId();
        }
        public void update ( final Alunos alunos){
            aluno.stream()
                    .filter(aln -> aln.getId().equals(alunos.getId()))
                    .forEach(aln -> aln.setNome(alunos.getNome()));
            aluno.stream()
                    .filter(aln -> aln.getId().equals(alunos.getId()))
                    .forEach(aln -> aln.setIdade(alunos.getIdade()));

        }
        public void delete (@PathVariable("id") Integer id){
            aluno.removeIf(aln -> aln.getId().equals(id));
        }


    public String findAluno(String nome) throws AlunoNaoEncontradoException{
        return findAll().stream()
                .filter(aln -> aln.equals(nome))
                .findFirst()
                .orElseThrow(() -> new AlunoNaoEncontradoException(nome));
    }
    public Integer findId(Integer id)throws IdNaoEncontradoException{
        return findAll().stream()
                .filter(aln -> aln.equals(id))
                .findFirst()
                .orElseThrow(() -> new IdNaoEncontradoException(id));
    }
}

